# Nebius JavaScript S3 Explorer (v2 alpha)

Note: this tool based on the AWS JavaScript S3 Explorer [S3 Explorer](https://github.com/awslabs/aws-js-s3-explorer/tree/v2-alpha).

Nebius JavaScript S3 Explorer (v2 alpha) is a JavaScript application that uses AWS's JavaScript SDK and S3 APIs to make the contents of an S3 bucket easy to browse via a web browser. We've created this to enable easier sharing and management of objects and data in S3 Object Storage.

The index.html, explorer.js, and explorer.css files in this bucket contain the entire application. A visitor to the index.html page is prompted to enter the name of an S3 bucket and optionally supply credentials. Upon supplying the required information, the contents of the bucket will be rendered on the page.

**Important**: unless you explicitly want everyone on the internet to be able to read your S3 bucket, you should ensure that your S3 bucket is **not** public. 

## Screenshots

Default starting view for public S3 bucket:
![Main screen][main-public]

[main-public]: https://raw.githubusercontent.com/awslabs/aws-js-s3-explorer/v2-alpha/screenshots/explorer-main-public.png

Default starting view for private S3 bucket:
![Main screen][main-private]

[main-private]: https://raw.githubusercontent.com/awslabs/aws-js-s3-explorer/v2-alpha/screenshots/explorer-main-private.png

View all objects in folder:
![Folder selected screen][folder]

[folder]: https://raw.githubusercontent.com/awslabs/aws-js-s3-explorer/v2-alpha/screenshots/explorer-folder.png

View all objects in bucket:
![Bucket traversal screen][bucket]

[bucket]: https://raw.githubusercontent.com/awslabs/aws-js-s3-explorer/v2-alpha/screenshots/explorer-bucket.png

## Deployment and Use

Note that in the general case, you are working with two distinct S3 buckets:

1. the S3 bucket hosting this tool, let's call it BUCKET1
2. the S3 bucket that you intend to use this tool to explore, let's call it BUCKET2

To deploy S3 Explorer, you have to do the following:

1. store index.html, explorer.css, and explorer.js in BUCKET1
2. apply an S3 bucket policy to BUCKET1 that allows unauthenticated read of the 3 files
3. [Enabling public access for bucket](https://nebius.com/il/docs/storage/operations/buckets/bucket-availability)
  * by going to the Nebius console at <https://console.il.nebius.com/> 
  * and select: Object Storage > Buckets > BUCKET1 > Settings:
  * Object read access: Public
  * Object listing access: Public
4. [Configure Static website hosting](https://nebius.com/il/docs/storage/operations/hosting/setup#hosting)

To launch and use S3 Explorer to explore BUCKET2 you have to do the following:

1. open the hosted index.html file in your browser at https://BUCKET1.website.il.nebius.cloud (when website was configured)
  * or open https://storage.il.nebius.cloud/BUCKET1/index.html
2. supply BUCKET2 as the bucket name
3. choose Private Bucket (I have NEBIUS credentials)
4. supply your Static access key credentials
5. click Query S3

More detailed configuration instructions follow.

### Configure the Bucket Hosting S3 Explorer

To launch the S3 Explorer, you need to make its files publicly readable. To do that, you will need to:
1. [Enabling public access for bucket](https://nebius.com/il/docs/storage/operations/buckets/bucket-availability)
2. [Configure Static website hosting](https://nebius.com/il/docs/storage/operations/hosting/setup#hosting)


### Enabling CORS

In order for S3 Explorer hosted in BUCKET1 to explore the contents of BUCKET2, BUCKET2 needs to have the proper Cross-Origin Resource Sharing (CORS) configuration allowing web pages hosted in BUCKET1 to make requests to BUCKET2. You can do this by going to the Nebius console at <https://console.il.nebius.com/> and select: Object Storage > Buckets > BUCKET2.

CORS defines a way for client web applications that are loaded in one domain to interact with resources in a different domain.

Note that CORS configurations do not, in and of themselves, authorize the user to perform any actions on the bucket. They simply enable the browser's security model to allow a request to S3. Actual permissions for the user must be configured either via bucket permissions (for public access), or IAM permissions (for private access).

#### CORS for Read-Only S3 Bucket

If you intend to allow read-only access from BUCKET1, which hosts S3 Explorer, to BUCKET2, then you will need to supply a CORS configuration on BUCKET2 that permits HEAD and GET operations, for example:

```
AllowedOrigins: https://storage.il.nebius.cloud, https://BUCKET1.website.il.nebius.cloud
AllowedMethods: HEAD, GET
AllowedHeaders: *
ExposeHeaders: ETag, x-amz-meta-myheader
MaxAgeSeconds: 3000
```

#### CORS for Writable S3 Bucket

If you intend to allow modifications to objects in BUCKET2, for example deleting existing objects or uploading new objects, then you will need to supply additional CORS configuration that permits PUT, POST and DELETE operations, for example:

```
AllowedOrigins: https://storage.il.nebius.cloud, https://BUCKET1.website.il.nebius.cloud
AllowedMethods: HEAD, GET, POST, PUT, DELETE
AllowedHeaders: *
ExposeHeaders: ETag, x-amz-meta-myheader
MaxAgeSeconds: 3000
```

### Static Website Hosting

Above, we indicated that users can access your index.html via one of two URLs:

* <https://storage.il.nebius.cloud/BUCKET-NAME/index.html> (path-style URL)
* <https://BUCKET-NAME.website.il.nebius.cloud/index.html> (virtual-hosted-style URL)

## Display Options

This application allows visitors to view the contents of a bucket via its folders or by listing out all objects in a bucket. The default view is by folder, but users can choose Initial View: Bucket in Settings to display all objects in the bucket. Note that viewing an entire bucket that contains many objects could overwhelm the browser. We&rsquo;ve successfully tested this application on a bucket with over 30,000 objects, but keep in mind that trying to list too many objects in a browser could lead to a poor user experience.

## Credentials

This tool can be used for publicly-readable S3 buckets (where no authentication is required) and for private S3 buckets (where authentication *is* required).

If you choose to explore a private S3 bucket then you will need to supply credentials. Credentials can be provided as:

* Static access key for object storage credentials: access key ID and secret access key
* [Creating static access keys](https://nebius.com/il/docs/iam/operations/sa/create-access-key)
